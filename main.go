package main

import (
	"log"

	"gitlab.com/ruben.van.der.veen/crud-api/app"
	"gitlab.com/ruben.van.der.veen/crud-api/config"
)

func main() {
	config := config.GetConfig()

	app := &app.App{}
	app.Initialize(config)
	log.Printf("crud-api succesfuly started")
	app.Run(":3001")
}
